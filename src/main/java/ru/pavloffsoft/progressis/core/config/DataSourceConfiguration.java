package ru.pavloffsoft.progressis.core.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import ru.pavloffsoft.progressis.core.tools.jdbc.IJdbc;
import ru.pavloffsoft.progressis.core.tools.jdbc.Jdbc;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfiguration {

    @Bean(name = "auth")
    @ConfigurationProperties(prefix = "spring.progress-auth")
    public DataSource auth() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "authJdbcTemplate")
    public IJdbc authJdbcTemplate(@Qualifier("auth") DataSource dataSource) {
        return new Jdbc(new JdbcTemplate(dataSource));
    }

    @Primary
    @Bean(name = "resource")
    @ConfigurationProperties(prefix = "spring.progress-resource")
    public DataSource resource() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "resourceJdbcTemplate")
    public IJdbc resourceJdbcTemplate(@Qualifier("resource") DataSource dataSource) {
        return new Jdbc(new JdbcTemplate(dataSource));
    }
}
