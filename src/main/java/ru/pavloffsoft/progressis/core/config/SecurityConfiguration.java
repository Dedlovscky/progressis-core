package ru.pavloffsoft.progressis.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import ru.pavloffsoft.progressis.core.components.SystemAuthenticationEntryPoint;
import ru.pavloffsoft.progressis.core.domain.users.types.Role;
import ru.pavloffsoft.progressis.core.security.JwtProvider;
import ru.pavloffsoft.progressis.core.security.SecurityConstants;
import ru.pavloffsoft.progressis.core.security.UrlKey;
import ru.pavloffsoft.progressis.core.security.components.JwtAuthenticationProvider;
import ru.pavloffsoft.progressis.core.security.filters.JwtAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired
    private SystemAuthenticationEntryPoint systemAuthenticationEntryPoint;
//    @Autowired
//    private JwtAuthenticationProvider jwtAuthenticationProvider;
    @Autowired
    private JwtProvider jwtProvider;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.anonymous().disable();
        http.cors().and()
                .formLogin().disable()
                .logout().disable()
                .csrf().disable()
                .httpBasic().disable()
                .authorizeRequests()
                // ВАЖНО!!! antPatterns определять от меньшего кол-ва полномочий к большему
//                    .antMatchers(SecurityConstants.SOURCES.get(Role.NONE)).permitAll()
                .antMatchers(SecurityConstants.PATTERNS.get(UrlKey.USER)).hasAnyRole(SecurityConstants.ROLES.get(UrlKey.USER))
                .antMatchers(SecurityConstants.PATTERNS.get(UrlKey.ADMINISTRATOR)).hasAnyRole(SecurityConstants.ROLES.get(UrlKey.ADMINISTRATOR))
                .antMatchers(SecurityConstants.PATTERNS.get(UrlKey.ARCHITECT)).hasAnyRole(SecurityConstants.ROLES.get(UrlKey.ARCHITECT))

                .anyRequest().authenticated()
                .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(systemAuthenticationEntryPoint)
                .and()
                    .addFilterAfter(new JwtAuthenticationFilter(authenticationManager(), jwtProvider), UsernamePasswordAuthenticationFilter.class)
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(new JwtAuthenticationProvider());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        CorsConfiguration config = new CorsConfiguration();
//        config.setAllowCredentials(true);
        config.addAllowedOrigin("*");
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");

        source.registerCorsConfiguration("/**", config);//new CorsConfiguration().applyPermitDefaultValues());

        return source;
    }
}
