package ru.pavloffsoft.progressis.core.controllers.v1;

import org.springframework.web.bind.annotation.*;
import ru.pavloffsoft.progressis.core.controllers.v1.code.EmployeesCode;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.pavloffsoft.progressis.core.services.personnel.IPersonnelService;
import ru.pavloffsoft.progressis.core.services.personnel.employees.IEmployeesService;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeesController {
    private final EmployeesCode employeesCode;

    public EmployeesController(IEmployeesService employeesService, IPersonnelService personnelService){
        employeesCode = new EmployeesCode(employeesService, personnelService);
    }

    @PostMapping("/add")
    public Response add(@RequestBody RequestEmployeeDTO employeeDTO,
                        @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return employeesCode.add(employeeDTO, systemUser);
    }

    @PutMapping("/edit")
    public Response edit(@RequestBody RequestEmployeeDTO employeeDTO,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return employeesCode.edit(employeeDTO, systemUser);
    }

    @DeleteMapping("/remove")
    public Response remove(@RequestParam("employeeId") ID employeeId,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return employeesCode.remove(employeeId, systemUser);
    }

    @DeleteMapping("/delete")
    public Response delete(@RequestParam("employeeId") ID employeeId,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return employeesCode.delete(employeeId, systemUser);
    }

    @GetMapping("/getEmployeeById")
    public Response getEmployeeById(@RequestParam("employeeId") ID employeeId,
                                    @RequestParam("withUser") boolean withUser,
                                    @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return employeesCode.getEmployee(employeeId, systemUser, withUser);
    }

    @GetMapping("/getEmployees")
    public Response getEmployees(@RequestAttribute(value = "systemUser") SystemUser systemUser){
        return employeesCode.getEmployees(systemUser);
    }
}
