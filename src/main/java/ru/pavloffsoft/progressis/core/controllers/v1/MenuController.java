package ru.pavloffsoft.progressis.core.controllers.v1;

import org.springframework.web.bind.annotation.*;
import ru.pavloffsoft.progressis.core.controllers.v1.code.MenuCode;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.domain.users.types.Role;
import ru.pavloffsoft.progressis.core.providers.HttpProvider;
import ru.pavloffsoft.progressis.core.services.menu.IMenuService;
import ru.pavloffsoft.progressis.core.tools.Response;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/menu")
public class MenuController {
    private final MenuCode menuCode;

    public MenuController(IMenuService menuService){
        menuCode = new MenuCode(menuService);
    }

    @GetMapping("/get")
    public Response get(@RequestAttribute(value = "systemUser") SystemUser systemUser) throws ExecutionException, InterruptedException {
        return menuCode.getMenu(systemUser);
    }
}
