package ru.pavloffsoft.progressis.core.controllers.v1;

import org.springframework.web.bind.annotation.*;
import ru.pavloffsoft.progressis.core.controllers.v1.code.UsersGroupsCode;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.dto.v1.usersGroups.RequestUserGroupDTO;
import ru.pavloffsoft.progressis.core.services.usersGroups.IUsersGroupsService;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

@RestController
@RequestMapping("/api/v1/usersGroups")
public class UsersGroupsController {
    private final UsersGroupsCode usersGroupsCode;

    public UsersGroupsController(IUsersGroupsService usersGroupsService){
        usersGroupsCode = new UsersGroupsCode(usersGroupsService);
    }

    @PostMapping("/add")
    public Response add(@RequestBody RequestUserGroupDTO userGroup,
                        @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return usersGroupsCode.add(userGroup, systemUser);
    }

    @PutMapping("/edit")
    public Response edit(@RequestBody RequestUserGroupDTO userGroup,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return usersGroupsCode.edit(userGroup, systemUser);
    }

    @DeleteMapping("/remove")
    public Response remove(@RequestParam("employeeId") ID userGroupId,
                           @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return usersGroupsCode.remove(userGroupId, systemUser);
    }

    @DeleteMapping("/delete")
    public Response delete(@RequestParam("employeeId") ID userGroupId,
                           @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return usersGroupsCode.delete(userGroupId, systemUser);
    }

    @GetMapping("/getUserGroup")
    public Response get(@RequestParam("employeeId") ID userGroupId,
                        @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return usersGroupsCode.getUserGroup(userGroupId, systemUser);
    }

    @GetMapping("/getUsersGroups")
    public Response get(@RequestAttribute(value = "systemUser") SystemUser systemUser){
        return usersGroupsCode.getUsersGroups(systemUser);
    }
}
