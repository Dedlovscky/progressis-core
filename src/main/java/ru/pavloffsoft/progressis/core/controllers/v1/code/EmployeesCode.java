package ru.pavloffsoft.progressis.core.controllers.v1.code;

import ru.pavloffsoft.progressis.core.domain.employees.Employee;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.pavloffsoft.progressis.core.dto.v1.employees.ResponseEmployeeDTO;
import ru.pavloffsoft.progressis.core.services.personnel.IPersonnelService;
import ru.pavloffsoft.progressis.core.services.personnel.employees.IEmployeesService;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import java.util.ArrayList;
import java.util.List;

public class EmployeesCode {
    private final IEmployeesService employeesService;
    private final IPersonnelService personnelService;

    public EmployeesCode(IEmployeesService employeesService, IPersonnelService personnelService){
        this.employeesService = employeesService;
        this.personnelService = personnelService;
    }

    public Response<Void> add(RequestEmployeeDTO employeeDTO, SystemUser systemUser) {
        return personnelService.add(employeeDTO, systemUser);
    }

    public Response<Void> edit(RequestEmployeeDTO employeeDTO, SystemUser systemUser) {
        return employeesService.edit(employeeDTO, systemUser);
    }

    public Response<Void> remove(ID employeeId, SystemUser systemUser) {
        return employeesService.remove(employeeId, systemUser.getCompanyId());
    }

    public Response<Void> delete(ID employeeId, SystemUser systemUser) {
        return employeesService.delete(employeeId, systemUser.getCompanyId());
    }

    public Response<ResponseEmployeeDTO> getEmployee(ID employeeId, SystemUser systemUser, boolean withUser){
        Employee employee = employeesService.getEmployee(employeeId, systemUser.getCompanyId(), withUser, false);

        ResponseEmployeeDTO employeeDTO = employee == null
                ? null
                : new ResponseEmployeeDTO(employee);

        return new Response<>(true, employeeDTO);
    }

    public Response<List<ResponseEmployeeDTO>> getEmployees(SystemUser systemUser) {
        List<ResponseEmployeeDTO> employeeDTOs = new ArrayList<>();
        List<Employee> employees = personnelService.getEmployees(systemUser.getCompanyId(), false);

        for (Employee employee : employees){
            ResponseEmployeeDTO employeeDTO = new ResponseEmployeeDTO(employee);

            employeeDTOs.add(employeeDTO);
        }

        return new Response<>(true, employeeDTOs);
    }
}
