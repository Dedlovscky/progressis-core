package ru.pavloffsoft.progressis.core.controllers.v1.code;

import ru.pavloffsoft.progressis.core.domain.menu.Menu;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.dto.v1.menu.MenuDTO;
import ru.pavloffsoft.progressis.core.services.menu.IMenuService;
import ru.pavloffsoft.progressis.core.tools.Response;

public class MenuCode {
    private final IMenuService menuService;

    public MenuCode(IMenuService menuService){
        this.menuService = menuService;
    }

    public Response getMenu(SystemUser systemUser){
        Menu menu = menuService.getMenu(systemUser.getRole());

        MenuDTO menuDTO = menu == null ? null : new MenuDTO(menu);

        return new Response<>(menuDTO);
    }
}
