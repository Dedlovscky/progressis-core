package ru.pavloffsoft.progressis.core.controllers.v1.code;

import ru.pavloffsoft.progressis.core.domain.groupUsers.GroupUser;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroup;
import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroupBlank;
import ru.pavloffsoft.progressis.core.dto.v1.groupUsers.GroupUserDTO;
import ru.pavloffsoft.progressis.core.dto.v1.usersGroups.RequestUserGroupDTO;
import ru.pavloffsoft.progressis.core.dto.v1.usersGroups.ResponseUserGroupDTO;
import ru.pavloffsoft.progressis.core.services.usersGroups.IUsersGroupsService;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import java.util.ArrayList;
import java.util.List;

public class UsersGroupsCode {
    private IUsersGroupsService usersGroupsService;

    public UsersGroupsCode(IUsersGroupsService usersGroupsService){
        this.usersGroupsService = usersGroupsService;
    }

    public Response<Void> add(RequestUserGroupDTO userGroup, SystemUser systemUser) {
        return usersGroupsService.add(new UserGroupBlank(userGroup), systemUser);
    }

    public Response<Void> edit(RequestUserGroupDTO userGroup, SystemUser systemUser) {
        return usersGroupsService.edit(new UserGroupBlank(userGroup), systemUser);
    }

    public Response<Void> remove(ID userGroupId, SystemUser systemUser) {
        return usersGroupsService.remove(userGroupId, systemUser.getCompanyId());
    }

    public Response<Void> delete(ID userGroupId, SystemUser systemUser) {
        return usersGroupsService.delete(userGroupId, systemUser.getCompanyId());
    }

    public Response<ResponseUserGroupDTO> getUserGroup(ID userGroupId, SystemUser systemUser){
        UserGroup userGroup = usersGroupsService.getUserGroup(userGroupId, systemUser.getCompanyId());

        if(userGroup == null) return null;

        List<GroupUserDTO> groupUserDTOs = getGroupUserDTOs(userGroup.getUsers());

        return new Response<>(new ResponseUserGroupDTO(userGroup.getId(), userGroup.getName(), groupUserDTOs));
    }

    public Response<List<ResponseUserGroupDTO>> getUsersGroups(SystemUser systemUser){
        List<UserGroup> userGroups = usersGroupsService.getUsersGroups(systemUser.getCompanyId(), false);

        List<ResponseUserGroupDTO> userGroupDTOs = new ArrayList<>();

        for (UserGroup userGroup : userGroups){
            List<GroupUserDTO> groupUserDTOs = getGroupUserDTOs(userGroup.getUsers());

            userGroupDTOs.add(new ResponseUserGroupDTO(userGroup.getId(), userGroup.getName(), groupUserDTOs));
        }

        return new Response<>(userGroupDTOs);
    }

    private List<GroupUserDTO> getGroupUserDTOs(List<GroupUser> groupUsers){
        List<GroupUserDTO> groupUserDTOs = new ArrayList<>();

        for (GroupUser groupUser : groupUsers){
            groupUserDTOs.add(new GroupUserDTO(groupUser));
        }

        return groupUserDTOs;
    }
}
