package ru.pavloffsoft.progressis.core.domain.employees;

import ru.pavloffsoft.progressis.core.domain.users.types.Gender;
import ru.pavloffsoft.progressis.core.tools.ID;

public class Employee {
    private ID id;
    private String lastName;
    private String firstName;
    private String patronymic;
    private Gender gender;

    public Employee(ID id, String lastName, String firstName, String patronymic, Gender gender) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender = gender;
    }

    public ID getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Gender getGender() {
        return gender;
    }
}
