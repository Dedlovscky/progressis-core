package ru.pavloffsoft.progressis.core.domain.employees;

import ru.pavloffsoft.progressis.core.domain.users.UserBlank;
import ru.pavloffsoft.progressis.core.domain.users.types.Gender;
import ru.pavloffsoft.progressis.core.dto.v1.employees.EmployeeDTO;
import ru.pavloffsoft.progressis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.pavloffsoft.progressis.core.dto.v1.users.RequestUserDTO;
import ru.pavloffsoft.progressis.core.tools.ID;

import javax.validation.constraints.NotNull;

public class EmployeeBlank {
    private ID employeeId;

    @NotNull(message = "Не указана фамилия")
    private String lastName;

    @NotNull(message = "Не указано имя")
    private String firstName;

    private String patronymic;
    private Gender gender;
    private UserBlank userBlank;

    public EmployeeBlank(RequestEmployeeDTO employeeDTO){
        this.employeeId = employeeDTO.getEmployeeId();
        this.lastName = employeeDTO.getLastName();
        this.firstName = employeeDTO.getFirstName();
        this.patronymic = employeeDTO.getPatronymic();
        this.gender = employeeDTO.getGender();
        this.userBlank = employeeDTO.getUser() == null ? null : new UserBlank(employeeDTO.getUser());
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Gender getGender() {
        return gender;
    }

    public UserBlank getUserBlank() {
        return userBlank;
    }
}
