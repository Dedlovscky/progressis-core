package ru.pavloffsoft.progressis.core.domain.groupUsers;

import ru.pavloffsoft.progressis.core.domain.employees.Employee;
import ru.pavloffsoft.progressis.core.domain.users.User;

public class GroupUser {
    private User user;
    private Employee employee;

    public GroupUser(User user, Employee employee) {
        this.user = user;
        this.employee = employee;
    }

    public User getUser() {
        return user;
    }

    public Employee getEmployee() {
        return employee;
    }
}
