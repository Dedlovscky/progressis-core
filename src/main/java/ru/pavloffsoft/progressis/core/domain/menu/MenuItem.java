package ru.pavloffsoft.progressis.core.domain.menu;

public class MenuItem {
    private int id;
    private String icon;
    private String title;
    private String cssClass;
    private String url;

    public MenuItem(int id, String icon, String title, String cssClass, String url) {
        this.id = id;
        this.icon = icon;
        this.title = title;
        this.cssClass = cssClass;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getCssClass() {
        return cssClass;
    }

    public String getUrl() {
        return url;
    }
}
