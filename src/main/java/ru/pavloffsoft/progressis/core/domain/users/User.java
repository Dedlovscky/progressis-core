package ru.pavloffsoft.progressis.core.domain.users;

import ru.pavloffsoft.progressis.core.domain.users.types.Role;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public class User {
    private ID id;
    private ID employeeId;
    private ID companyId;
    private String username;
    private Role role;
    private List<ID> userGroupIds;

    public User(ID id, ID employeeId, ID companyId, String username, Role role, List<ID> userGroupIds) {
        this.id = id;
        this.employeeId = employeeId;
        this.companyId = companyId;
        this.username = username;
        this.role = role;
        this.userGroupIds = userGroupIds;
    }

    public ID getId() {
        return id;
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public String getUsername() {
        return username;
    }

    public Role getRole() {
        return role;
    }

    public List<ID> getUserGroupIds() {
        return userGroupIds;
    }
}
