package ru.pavloffsoft.progressis.core.domain.users.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Gender {
    NONE("Не выбрано",255),
    MALE("Мужсккой",0),
    FEMALE("Женский",1);

    private String displayValue;
    private int number;

    Gender(String displayValue, int number) {
        this.displayValue = displayValue;
        this.number = number;
    }

    @JsonCreator
    public static Gender getGender(int value){
        for (Gender gender : Gender.values()) {
            if (gender.number == value) return gender;
        }

        return Gender.NONE;
    }

    @JsonValue
    public int getNumber() {
        return number;
    }
}
