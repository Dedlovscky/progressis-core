package ru.pavloffsoft.progressis.core.dto.v1.menu;

import ru.pavloffsoft.progressis.core.domain.menu.Menu;
import ru.pavloffsoft.progressis.core.domain.menu.MenuItem;

import java.util.List;

public class MenuDTO {
    private List<MenuItem> menuItems;

    public MenuDTO(Menu menu) {
        this.menuItems = menu.getMenuItems();
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }
}
