package ru.pavloffsoft.progressis.core.dto.v1.menu;

import ru.pavloffsoft.progressis.core.domain.menu.MenuItem;

public class MenuItemDTO {
    private int id;
    private String icon;
    private String title;
    private String cssClass;
    private String url;

    public MenuItemDTO(MenuItem menuItem){
        this.id = menuItem.getId();
        this.icon = menuItem.getIcon();
        this.title = menuItem.getTitle();
        this.cssClass = menuItem.getCssClass();
        this.url = menuItem.getUrl();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
