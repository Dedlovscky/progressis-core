package ru.pavloffsoft.progressis.core.dto.v1.users;

import ru.pavloffsoft.progressis.core.domain.users.types.Role;
import ru.pavloffsoft.progressis.core.tools.ID;

public final class ResponseUserDTO extends UserDTO {
    public ResponseUserDTO(ID userId, String username, Role role) {
        super(userId, username, role);
    }
}
