package ru.pavloffsoft.progressis.core.dto.v1.users;

import ru.pavloffsoft.progressis.core.domain.users.types.Role;
import ru.pavloffsoft.progressis.core.tools.ID;

public abstract class UserDTO {
    private ID userId;
    private String username;
    private Role role;

    UserDTO(ID userId, String username, Role role) {
        this.userId = userId;
        this.username = username;
        this.role = role;
    }

    public ID getUserId() {
        return userId;
    }

    public void setUserId(ID userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
