package ru.pavloffsoft.progressis.core.dto.v1.usersGroups;

import ru.pavloffsoft.progressis.core.tools.ID;

public abstract class UserGroupDTO {
    private ID id;
    private String name;

    UserGroupDTO(ID id, String name) {
        this.id = id;
        this.name = name;
    }

    public ID getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
