package ru.pavloffsoft.progressis.core.providers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Component
public class HttpProvider {
    private final static HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public static String get(String url, String token) throws InterruptedException, ExecutionException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", String.format("OAuth %s", token))
                .GET()
                .uri(URI.create(url))
                .build();

        CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

        return response.thenApply(HttpResponse::body).get();
    }

    public static String get(String url, Map<String, String> params, String token) throws InterruptedException, ExecutionException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", String.format("OAuth %s", token))
                .GET()
                .uri(URI.create(getUrlParams(url, params)))
                .build();

        CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

        return response.thenApply(HttpResponse::body).get();
    }

    public static <T> String post(String url, T object, String token) throws InterruptedException, ExecutionException, JsonProcessingException {
        String jsonObject = new ObjectMapper().writeValueAsString(object);

        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", String.format("OAuth %s", token))
                .POST(HttpRequest.BodyPublishers.ofString(jsonObject))
                .uri(URI.create(url))
                .build();

        CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

        return response.thenApply(HttpResponse::body).get();
    }

    public static String post(String url, String params) throws InterruptedException, IOException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/x-www-form-urlencoded")
                .POST(HttpRequest.BodyPublishers.ofString(params))
                .uri(URI.create(url))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        return response.body();
    }

    public static <T> String post(String url, Map<String, String> params, Path path, String token) throws InterruptedException, ExecutionException, FileNotFoundException {

        HttpRequest request = HttpRequest.newBuilder()
                .header("Content-Type", "application/json")
                .header("Authorization", String.format("OAuth %s", token))
                .POST(HttpRequest.BodyPublishers.ofFile(path))
                .uri(URI.create(getUrlParams(url, params)))
                .build();

        CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

        return response.thenApply(HttpResponse::body).get();
    }

    public static String put(String url, Path path) throws InterruptedException, ExecutionException, FileNotFoundException {
        HttpRequest request = HttpRequest.newBuilder()
                .PUT(HttpRequest.BodyPublishers.ofFile(path))
                .uri(URI.create(url))
                .build();

        CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

        return response.thenApply(HttpResponse::body).get();
    }

    public static int put(String url, Map<String, String> params, String token) throws InterruptedException, ExecutionException {
        HttpRequest request = HttpRequest.newBuilder()
                .header("Authorization", String.format("OAuth %s", token))
                .PUT(HttpRequest.BodyPublishers.noBody())
                .uri(URI.create(getUrlParams(url, params)))
                .build();

        CompletableFuture<HttpResponse<String>> response =
                httpClient.sendAsync(request, HttpResponse.BodyHandlers.ofString());

        String d = response.thenApply(HttpResponse::body).get();

        return response.thenApply(HttpResponse::statusCode).get();
    }

    private static String getUrlParams(String url, Map<String, String> params){
        StringBuilder urlWithParams = new StringBuilder(url);

        int countParams = 0;

        for (Map.Entry<String, String> entry : params.entrySet()){
            String parameter = String.format("%s=%s", entry.getKey(), entry.getValue());
            String sign = countParams == 0 ? "?" : "&";

            urlWithParams.append(sign).append(parameter);

            countParams++;
        }

        return urlWithParams.toString();
    }
}
