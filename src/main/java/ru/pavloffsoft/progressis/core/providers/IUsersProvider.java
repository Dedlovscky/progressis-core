package ru.pavloffsoft.progressis.core.providers;

import ru.pavloffsoft.progressis.core.dto.v1.users.RequestUserDTO;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public interface IUsersProvider {
    void add(RequestUserDTO requestUserDTO);
    void edit(RequestUserDTO requestUserDTO);
    void removeById(ID userId, ID companyId);
    void deleteById(ID userId, ID companyId);
    void removeByEmployeeId(ID employeeId, ID companyId);
    void deleteByEmployeeId(ID employeeId, ID companyId);

    UserDB getUser(ID employeeId, boolean isRemoved);

    List<UserDB> getUsers(List<ID> userIds);
    List<UserDB> getUsers(ID companyId, boolean isRemoved);
}
