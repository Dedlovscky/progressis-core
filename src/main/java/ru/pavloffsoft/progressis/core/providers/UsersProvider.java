package ru.pavloffsoft.progressis.core.providers;

import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.core.dto.v1.users.RequestUserDTO;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

@Service
public class UsersProvider implements IUsersProvider {
    @Override
    public void add(RequestUserDTO requestUserDTO) {

    }

    @Override
    public void edit(RequestUserDTO requestUserDTO) {

    }

    @Override
    public void removeById(ID userId, ID companyId) {

    }

    @Override
    public void deleteById(ID userId, ID companyId) {

    }

    @Override
    public void removeByEmployeeId(ID employeeId, ID companyId) {

    }

    @Override
    public void deleteByEmployeeId(ID employeeId, ID companyId) {

    }

    @Override
    public UserDB getUser(ID employeeId, boolean isRemoved) {
        return null;
    }

    @Override
    public List<UserDB> getUsers(List<ID> userIds) {
        return null;
    }

    @Override
    public List<UserDB> getUsers(ID companyId, boolean isRemoved) {
        return null;
    }
}
