package ru.pavloffsoft.progressis.core.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;

import java.util.Collection;

public class JwtAuthentication implements Authentication {
    private String token;
    private Collection<? extends GrantedAuthority> grantedAuthorities;
    private boolean isAuthenticated;
    private JwtUser principal;
    private SystemUser systemUser;

    public JwtAuthentication(){}

    public JwtAuthentication(String token){
        this.token = token;
    }

    public JwtAuthentication(Collection<? extends GrantedAuthority> grantedAuthorities, boolean isAuthenticated, JwtUser principal, SystemUser systemUser) {
        this.grantedAuthorities = grantedAuthorities;
        this.isAuthenticated = isAuthenticated;
        this.principal = principal;
        this.systemUser = systemUser;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorities;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }

    @Override
    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        this.isAuthenticated = isAuthenticated;
    }

    @Override
    public String getName() {
        return principal == null ? null : principal.getUsername();
    }

    public String getToken() {
        return token;
    }

    public SystemUser getSystemUser() {
        return systemUser;
    }
}
