package ru.pavloffsoft.progressis.core.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;

@Component
public class JwtProvider {
    public SystemUser getSystemUser(String token){
        SystemUser systemUser = null;

        if (!StringUtils.isEmpty(token) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();
            Jws<Claims> parsedToken;

            try {
                parsedToken = Jwts.parser()
                        .setSigningKey(signingKey)
                        .parseClaimsJws(token.replace("Bearer ", ""));
            }catch (Exception e){
                return null;
            }

            systemUser = new ObjectMapper().convertValue(parsedToken.getBody(), SystemUser.class);
        }

        return systemUser;
    }
}
