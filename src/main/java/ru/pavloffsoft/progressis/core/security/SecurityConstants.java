package ru.pavloffsoft.progressis.core.security;

import ru.pavloffsoft.progressis.core.domain.users.types.Role;

import java.util.Map;

public final class SecurityConstants {
    public static final String JWT_SECRET = "UGTuVBVMbbbSQvmsuvaEg9kNb8FDuRGqed3SjErVsbNhJ8TpTLzXdbdFhJQs9WkS";
    public static final long ACCESS_JWT_EXPIRATION_TIME = 2_880_000; //48 минут
    public static final long REFRESH_JWT_EXPIRATION_TIME = 864_000_000; // 10 days
    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String TOKEN_TYPE = "JWT";
    public static final String TOKEN_ISSUER = "secure-api";
    public static final String TOKEN_AUDIENCE = "secure-app";

    /**
     * Список роли имеющие доступ к конкретным ресурсам
     * Role - ключ
     * String[] - массив URLs
     */
    public static final Map<UrlKey, String[]> ROLES = Map.ofEntries(
            Map.entry(UrlKey.USER, new String[] {
                    Role.USER.name(),
                    Role.ADMINISTRATOR.name(),
                    Role.ARCHITECT.name()
            }),
            Map.entry(UrlKey.ADMINISTRATOR, new String[] {
                    Role.ADMINISTRATOR.name(),
                    Role.ARCHITECT.name()
            }),
            Map.entry(UrlKey.ARCHITECT, new String[] {
                    Role.ARCHITECT.name()
            })
    );

    public static final Map<UrlKey, String[]> PATTERNS = Map.ofEntries(
            Map.entry(UrlKey.USER, new String[] {
                    "/api/v1/menu/**",
                    "/api/v1/employees/**",
            }),
            Map.entry(UrlKey.ADMINISTRATOR, new String[] {

            }),
            Map.entry(UrlKey.ARCHITECT, new String[] {

            })
    );
}
