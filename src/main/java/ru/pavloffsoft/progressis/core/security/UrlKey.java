package ru.pavloffsoft.progressis.core.security;

public enum UrlKey {
    USER,
    ADMINISTRATOR,
    ARCHITECT
}
