package ru.pavloffsoft.progressis.core.security.components;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import ru.pavloffsoft.progressis.core.security.JwtAuthentication;
import ru.pavloffsoft.progressis.core.security.JwtUser;

@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        JwtAuthentication jwtAuthentication = (JwtAuthentication) authentication;

        return new JwtAuthentication(authentication.getAuthorities(),true,
                (JwtUser) jwtAuthentication.getPrincipal(), jwtAuthentication.getSystemUser());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(JwtAuthentication.class);
    }
}
