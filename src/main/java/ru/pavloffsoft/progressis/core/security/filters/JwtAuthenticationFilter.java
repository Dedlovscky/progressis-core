package ru.pavloffsoft.progressis.core.security.filters;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.security.JwtAuthentication;
import ru.pavloffsoft.progressis.core.security.JwtProvider;
import ru.pavloffsoft.progressis.core.security.JwtUser;
import ru.pavloffsoft.progressis.core.security.SecurityConstants;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Фильтр авторизации запросов
 */
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private JwtProvider jwtProvider;
    private SystemUser systemUser;
    private AuthenticationManager authenticationManager;

    public JwtAuthenticationFilter(AuthenticationManager authenticationManager, JwtProvider jwtProvider){
        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        JwtAuthentication jwtAuthentication = getJwtAuthentication(request);

        request.setAttribute("systemUser", systemUser);

        SecurityContextHolder.getContext().setAuthentication(jwtAuthentication);

        filterChain.doFilter(request, response);
    }

    private JwtAuthentication getJwtAuthentication(HttpServletRequest request) {
        String token = request.getHeader(SecurityConstants.TOKEN_HEADER);

        systemUser = jwtProvider.getSystemUser(token);

        JwtUser jwtUser = new JwtUser(systemUser.getUserId().toString(), systemUser.getCompanyId().toSqlString(), systemUser.getGrantedAuthority());

        if(systemUser == null) return null;

        return new JwtAuthentication(jwtUser.getAuthorities(), true, jwtUser, systemUser);
    }
}
