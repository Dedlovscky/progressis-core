package ru.pavloffsoft.progressis.core.services.menu;

import ru.pavloffsoft.progressis.core.domain.menu.Menu;
import ru.pavloffsoft.progressis.core.domain.users.types.Role;

public interface IMenuService {
    Menu getMenu(Role role);
}
