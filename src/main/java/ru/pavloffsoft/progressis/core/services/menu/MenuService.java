package ru.pavloffsoft.progressis.core.services.menu;

import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.core.domain.menu.Menu;
import ru.pavloffsoft.progressis.core.domain.menu.MenuItem;
import ru.pavloffsoft.progressis.core.domain.users.types.Role;

import java.util.List;

@Service
public class MenuService implements IMenuService {

    @Override
    public Menu getMenu(Role role){
        switch (role){
            case ARCHITECT: return getArchitectMenu();
            case ADMINISTRATOR: return getAdministratorMenu();
            case USER: return getUserMenu();
            default: return null;
        }
    }

    private Menu getArchitectMenu(){
        List<MenuItem> menuItems = List.of(
                new MenuItem(0, "fas fa-desktop fa-3x", "Рабочий стол", "active-item", "/main/desktop"),
                new MenuItem(1, "far fa-question-circle fa-3x", "Помощь", "", ""),
                new MenuItem(2, "fas fa-users fa-3x", "Сотрудники", "", "/main/staff"),
                new MenuItem(3, "far fa-calendar-check fa-3x", "Дела", "", "/main/affairs"),
                new MenuItem(4, "far fa-chart-bar fa-3x", "Отчеты", "", ""),
                new MenuItem(5, "fas fa-tasks fa-3x", "Задачи", "", ""),
                new MenuItem(6, "fas fa-briefcase fa-3x", "Сделки", "", ""),
                new MenuItem(7, "far fa-address-book fa-3x", "Клиенты", "", ""),
                new MenuItem(8, "fas fa-user-friends fa-3x", "Группы пользователей", "", "/main/users-groups")
        );

        return new Menu(menuItems);
    }

    private Menu getAdministratorMenu(){
        List<MenuItem> menuItems = List.of(
                new MenuItem(0, "fas fa-desktop fa-3x", "Рабочий стол", "active-item", "/main/desktop"),
                new MenuItem(1, "far fa-question-circle fa-3x", "Помощь", "", ""),
                new MenuItem(2, "fas fa-users fa-3x", "Сотрудники", "", "/main/staff"),
                new MenuItem(3, "far fa-calendar-check fa-3x", "Дела", "", "/main/affairs"),
                new MenuItem(4, "far fa-chart-bar fa-3x", "Отчеты", "", ""),
                new MenuItem(5, "fas fa-tasks fa-3x", "Задачи", "", ""),
                new MenuItem(6, "fas fa-briefcase fa-3x", "Сделки", "", ""),
                new MenuItem(7, "far fa-address-book fa-3x", "Клиенты", "", ""),
                new MenuItem(8, "fas fa-user-friends fa-3x", "Группы пользователей", "", "/main/users-groups")
        );

        return new Menu(menuItems);
    }

    private Menu getUserMenu(){
        List<MenuItem> menuItems = List.of(
                new MenuItem(0, "fas fa-desktop fa-3x", "Рабочий стол", "active-item", "/main/desktop"),
                new MenuItem(1, "far fa-question-circle fa-3x", "Помощь", "", ""),
                new MenuItem(2, "fas fa-users fa-3x", "Сотрудники", "", "/main/staff"),
                new MenuItem(3, "far fa-calendar-check fa-3x", "Дела", "", "/main/affairs"),
                new MenuItem(4, "far fa-chart-bar fa-3x", "Отчеты", "", ""),
                new MenuItem(5, "fas fa-tasks fa-3x", "Задачи", "", ""),
                new MenuItem(6, "fas fa-briefcase fa-3x", "Сделки", "", ""),
                new MenuItem(7, "far fa-address-book fa-3x", "Клиенты", "", "")
        );

        return new Menu(menuItems);
    }
}
