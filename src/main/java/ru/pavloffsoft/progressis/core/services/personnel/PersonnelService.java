package ru.pavloffsoft.progressis.core.services.personnel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.core.domain.employees.Employee;
import ru.pavloffsoft.progressis.core.domain.employees.EmployeeBlank;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.domain.users.User;
import ru.pavloffsoft.progressis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.pavloffsoft.progressis.core.services.personnel.converters.EmployeesConverter;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.IPersonnelRepository;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDB;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.BlankModelValidator;
import ru.pavloffsoft.progressis.core.tools.DateTime;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonnelService implements IPersonnelService {
    @Autowired
    private IPersonnelRepository personnelRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Response<Void> add(RequestEmployeeDTO requestEmployeeDTO, SystemUser systemUser) {
        BlankModelValidator validator = new BlankModelValidator();

        EmployeeBlank employeeBlank = new EmployeeBlank(requestEmployeeDTO);

        if(!validator.isBlankModelValid(employeeBlank))
            return new Response<>(false, "Проверьте правильность введенных данных сотрудника");

        DateTime currentDateTime = DateTime.NowUTC();

        EmployeeDB employeeDB = new EmployeeDB(ID.NewId(), systemUser.getCompanyId(), employeeBlank.getLastName(), employeeBlank.getFirstName(),
                employeeBlank.getPatronymic(), employeeBlank.getGender(), systemUser.getUserId(), DateTime.NowUTC());

        if(employeeBlank.getUserBlank() == null){
            personnelRepository.add(employeeDB);
        }else {
            if(!validator.isBlankModelValid(employeeBlank.getUserBlank()))
                return new Response<>(false, "Проверьте правильность введенных данных пользователя");

            String password = passwordEncoder.encode(employeeBlank.getUserBlank().getPassword());

            UserDB userDB = new UserDB(ID.NewId(), systemUser.getCompanyId(), employeeDB.getId(), employeeBlank.getUserBlank().getRole(),
                    new ArrayList<>(0), employeeBlank.getUserBlank().getUsername(), password, systemUser.getUserId(), currentDateTime);

            personnelRepository.add(employeeDB, userDB);
        }

        return new Response<>(true, "Новый сотрудник добавлен успешно");
    }

    @Override
    public Response<Void> edit(RequestEmployeeDTO requestEmployeeDTO, SystemUser systemUser) {
        return null;
    }

    @Override
    public Response<Void> remove(ID employeeId, ID companyId) {
        return null;
    }

    @Override
    public Response<Void> delete(ID employeeId, ID companyId) {
        return null;
    }

    @Override
    public Employee getEmployee(ID employeeId, ID companyId, boolean isRemoved) {
        return null;
    }

    @Override
    public List<Employee> getEmployees(ID companyId, boolean isRemoved) {
        if(companyId == null) return new ArrayList<>(0);

        List<EmployeeDB> employeeDBs = personnelRepository.getEmployees(companyId, isRemoved);

        if(employeeDBs.size() == 0) return new ArrayList<>(0);

        return EmployeesConverter.convert(employeeDBs);
    }

    @Override
    public List<Employee> getEmployees(List<ID> employeeIds) {
        return null;
    }

    @Override
    public List<User> getUsers(List<ID> userIds) {
        return null;
    }
}
