package ru.pavloffsoft.progressis.core.services.personnel.converters;

import ru.pavloffsoft.progressis.core.domain.users.User;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;

import java.util.ArrayList;
import java.util.List;

public final class UsersConverter {
    public static User convert(UserDB userDB){
        return new User(userDB.getId(), userDB.getEmployeeId(), userDB.getCompanyId(), userDB.getUsername(), userDB.getRole(), userDB.getUserGroupIds());
    }

    public static List<User> convert(List<UserDB> userDBs){
        List<User> users = new ArrayList<>();

        for (UserDB userDB : userDBs){
            users.add(convert(userDB));
        }

        return users;
    }
}
