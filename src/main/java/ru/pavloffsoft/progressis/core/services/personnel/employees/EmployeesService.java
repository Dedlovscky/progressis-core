package ru.pavloffsoft.progressis.core.services.personnel.employees;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.core.domain.employees.Employee;
import ru.pavloffsoft.progressis.core.domain.employees.EmployeeBlank;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.pavloffsoft.progressis.core.providers.IUsersProvider;
import ru.pavloffsoft.progressis.core.services.personnel.converters.EmployeesConverter;
import ru.pavloffsoft.progressis.core.services.personnel.employees.repositories.IEmployeesRepository;
import ru.pavloffsoft.progressis.core.services.personnel.users.IUsersService;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDB;
import ru.pavloffsoft.progressis.core.services.personnel.users.repositories.IUsersRepository;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.DateTime;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class EmployeesService implements IEmployeesService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private IUsersRepository usersRepository;
    @Autowired
    private IEmployeesRepository employeesRepository;
    @Autowired
    private IUsersService usersService;
    @Autowired
    private IUsersProvider usersProvider;

    @Override
    public Response<Void> add(RequestEmployeeDTO requestEmployeeDTO, SystemUser systemUser) {
        EmployeeBlank employeeBlank = new EmployeeBlank(requestEmployeeDTO);

        String message = validateEmployeeBlank(employeeBlank);

        if(!message.isBlank()) return new Response<>(false, message);

        EmployeeDB employeeDB = new EmployeeDB(ID.NewId(), systemUser.getCompanyId(), employeeBlank.getLastName(), employeeBlank.getFirstName(),
                employeeBlank.getPatronymic(), employeeBlank.getGender(), systemUser.getUserId(), DateTime.NowUTC());

//        if(re.getUserBlank() != null){
//            String password = passwordEncoder.encode(blank.getUserBlank().getPassword());
//
//            UserDB userDB = new UserDB(ID.NewId(), systemUser.getCompanyId(), employeeDB.getId(), blank.getUserBlank().getRole(),
//                    blank.getUserBlank().getUsername(), password, systemUser.getUserId(), currentDateTime);
//
//            usersRepository.add(userDB);
//        }

        usersProvider.add(requestEmployeeDTO.getUser());
        employeesRepository.add(employeeDB);

        return new Response<>(true, "Новый сотрудник добавлен успешно");
    }

    @Override
    public Response<Void> edit(RequestEmployeeDTO requestEmployeeDTO, SystemUser systemUser) {
        EmployeeBlank employeeBlank = new EmployeeBlank(requestEmployeeDTO);

        String message = validateEmployeeBlank(employeeBlank);

        if(!message.isBlank()) return new Response<>(false, message);

        EmployeeDB employeeDB = employeesRepository.getEmployee(employeeBlank.getEmployeeId(), systemUser.getCompanyId(), false);

        if(employeeDB == null) return new Response<>(false, "Сотрудника не существует");

        DateTime currentDateTimeUTC = DateTime.NowUTC();

        employeeDB.setLastName(employeeBlank.getLastName());
        employeeDB.setFirstName(employeeBlank.getFirstName());
        employeeDB.setPatronymic(employeeBlank.getPatronymic());
        employeeDB.setGender(employeeBlank.getGender());
        employeeDB.setModifiedUserId(systemUser.getUserId());
        employeeDB.setModifiedDateTimeUTC(currentDateTimeUTC);

//        if(employeeBlank.getUserBlank() != null){
////
////            UserDB userDB = usersRepository.getUser(employeeBlank.getEmployeeId(), false);
////
////            if(userDB == null){
////                userDB = new UserDB(ID.NewId(), systemUser.getCompanyId(), employeeDB.getId(), employeeBlank.getUserBlank().getRole(),
////                        employeeBlank.getUserBlank().getUsername(), employeeBlank.getUserBlank().getPassword(), systemUser.getUserId(), currentDateTimeUTC);
////
////                usersRepository.add(userDB);
////
////            }else {
////                boolean hasUsername = employeeBlank.getUserBlank().getUsername() != null && !employeeBlank.getUserBlank().getUsername().isBlank();
////                boolean hasPassword = employeeBlank.getUserBlank().getPassword() != null && !employeeBlank.getUserBlank().getPassword().isBlank();
////
////                String username = hasUsername ? employeeBlank.getUserBlank().getUsername() : userDB.getUsername();
////                String password = hasPassword ? employeeBlank.getUserBlank().getPassword() : userDB.getPassword();
////
////                userDB.setUsername(username);
////                userDB.setPassword(password);
////                userDB.setRole(employeeBlank.getUserBlank().getRole());
////                userDB.setModifiedUserId(systemUser.getUserId());
////                userDB.setModifiedDateTimeUTC(currentDateTimeUTC);
////
////                usersRepository.edit(userDB);
////            }
////        }

        usersProvider.edit(requestEmployeeDTO.getUser());
        employeesRepository.edit(employeeDB);

        return new Response<>(true, "Данные о сотруднике сохранены успешно");
    }

    @Override
    public Response<Void> remove(ID employeeId, ID companyId){
        if(employeeId == null || companyId == null) new Response<>(false, "Нет данных для удаления");

        employeesRepository.remove(employeeId, companyId);
        usersRepository.removeByEmployeeId(employeeId, companyId);

        return new Response<>(true, "Сотрудник удален успешно");
    }

    @Override
    public Response<Void> delete(ID employeeId, ID companyId){
        if(employeeId == null || companyId == null) new Response<>(false, "Нет данных для удаления");

        employeesRepository.delete(employeeId, companyId);
        usersRepository.deleteByEmployeeId(employeeId, companyId);

        return new Response<>(true, "Сотрудник удален успешно");
    }

    @Override
    public Employee getEmployee(ID employeeId, ID companyId, boolean withUser, boolean isRemoved) {
        if(employeeId == null || companyId == null) return null;

        EmployeeDB employeeDB = employeesRepository.getEmployee(employeeId, companyId, isRemoved);

        if(employeeDB == null) return null;

        return EmployeesConverter.convert(employeeDB);
    }

    @Override
    public List<Employee> getEmployees(ID companyId, boolean isRemoved) {
        if(companyId == null) return new ArrayList<>(0);

        List<EmployeeDB> employeeDBs = employeesRepository.getEmployees(companyId, isRemoved);

        if(employeeDBs.size() == 0) return new ArrayList<>(0);

        return EmployeesConverter.convert(employeeDBs);
    }

    @Override
    public List<Employee> getEmployees(List<ID> employeeIds, boolean withUser) {
        if(employeeIds.size() == 0) return new ArrayList<>(0);

        List<EmployeeDB> employeeDBs = employeesRepository.getEmployees(employeeIds);

        if(employeeDBs.size() == 0) return new ArrayList<>(0);

        List<UserDB> userDBs = new ArrayList<>();

//        if(withUser){
//            List<ID> userIds = employeeDBs.stream().map(Em)
//
//            users = usersService.getUsers()
//        }
//
//        return userDB == null
//                ? EmployeesConverter.convert(employeeDB)
//                : EmployeesConverter.convert(employeeDB, userDB);

        return EmployeesConverter.convert(employeeDBs);
    }

    private String validateEmployeeBlank(EmployeeBlank employeeBlank){
        if(employeeBlank == null) return "Нет данных для сохранения";

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<EmployeeBlank>> result = validator.validate(employeeBlank);

        for (ConstraintViolation<EmployeeBlank> validObject : result){
            if(!validObject.getMessage().isBlank()) return validObject.getMessage();
        }

        return "";
    }
}
