package ru.pavloffsoft.progressis.core.services.personnel.employees;

import ru.pavloffsoft.progressis.core.domain.employees.Employee;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import java.util.List;

public interface IEmployeesService {
    Response<Void> add(RequestEmployeeDTO requestEmployeeDTO, SystemUser systemUser);
    Response<Void> edit(RequestEmployeeDTO requestEmployeeDTO, SystemUser systemUser);
    Response<Void> remove(ID employeeId, ID companyId);
    Response<Void> delete(ID employeeId, ID companyId);

    Employee getEmployee(ID employeeId, ID companyId, boolean withUser, boolean isRemoved);
    List<Employee> getEmployees(ID companyId, boolean isRemoved);
    List<Employee> getEmployees(List<ID> employeeIds, boolean withUser);
}
