package ru.pavloffsoft.progressis.core.services.personnel.employees.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDB;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDBRM;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Sql;
import ru.pavloffsoft.progressis.core.tools.jdbc.IJdbc;

import java.util.List;

@Repository
public class EmployeesRepository implements IEmployeesRepository {

    @Autowired
    @Qualifier("resourceJdbcTemplate")
    private IJdbc jdbcTemplate;

    @Override
    public void add(EmployeeDB employeeDB) {
        String sql = Sql.read("/employees/repositories/sql/employees_add.sql");
        sql = sql.replaceAll("p_id", String.format("'%s'", employeeDB.getId()));
        sql = sql.replaceAll("p_companyId", String.format("'%s'", employeeDB.getCompanyId()));
        sql = sql.replaceAll("p_lastName", String.format("'%s'", employeeDB.getLastName()));
        sql = sql.replaceAll("p_firstName", String.format("'%s'", employeeDB.getFirstName()));
        sql = sql.replaceAll("p_patronymic", String.format("'%s'", employeeDB.getPatronymic()));
        sql = sql.replaceAll("p_gender", String.format("%s", employeeDB.getGender().getNumber()));
        sql = sql.replaceAll("p_createdUserId", String.format("'%s'", employeeDB.getCreatedUserId()));
        sql = sql.replaceAll("p_createdDateTimeUTC", String.format("'%s'", employeeDB.getCreatedDateTimeUTC().toSqlString()));
        sql = sql.replaceAll("p_modifiedUserId", "NULL");
        sql = sql.replaceAll("p_modifiedDateTimeUTC", "NULL");
        sql = sql.replaceAll("p_isRemoved", "FALSE");

        jdbcTemplate.execute(sql);
    }

    @Override
    public void edit(EmployeeDB employeeDB) {
        String sql = Sql.read("/employees/repositories/sql/employees_edit.sql");
        sql = sql.replaceAll("p_id", String.format("'%s'", employeeDB.getId()));
        sql = sql.replaceAll("p_lastName", String.format("'%s'", employeeDB.getLastName()));
        sql = sql.replaceAll("p_firstName", String.format("'%s'", employeeDB.getFirstName()));
        sql = sql.replaceAll("p_patronymic", String.format("'%s'", employeeDB.getPatronymic()));
        sql = sql.replaceAll("p_gender", String.format("%s", employeeDB.getGender().getNumber()));
        sql = sql.replaceAll("p_modifiedUserId", String.format("'%s'", employeeDB.getModifiedUserId()));
        sql = sql.replaceAll("p_modifiedDateTimeUTC", String.format("'%s'", employeeDB.getModifiedDateTimeUTC().toSqlString()));

        jdbcTemplate.execute(sql);
    }

    @Override
    public void remove(ID employeeId, ID companyId){
        String sql = Sql.read("/employees/repositories/sql/employees_remove.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        jdbcTemplate.execute(sql);
    }

    @Override
    public void delete(ID employeeId, ID companyId){
        String sql = Sql.read("/employees/repositories/sql/employees_delete.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        jdbcTemplate.execute(sql);
    }

    @Override
    public EmployeeDB getEmployee(ID employeeId, ID companyId, boolean isRemoved) {
        String sql = Sql.read("/employees/repositories/sql/employees_getByCompanyId_EmployeeId.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));
        sql = sql.replaceAll("p_isRemoved", String.format("%s", isRemoved));

        return jdbcTemplate.get(sql, new EmployeeDBRM());
    }

    @Override
    public List<EmployeeDB> getEmployees(ID companyId, boolean isRemoved) {
        String sql = Sql.read("/employees/repositories/sql/employees_getByCompanyId.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_isRemoved", String.format("%s", isRemoved));

        return jdbcTemplate.getList(sql, new EmployeeDBRM());
    }

    @Override
    public List<EmployeeDB> getEmployees(List<ID> employeeIds) {
        String sql = Sql.read("/employees/repositories/sql/employees_getByIds.sql");
        sql = sql.replaceAll("p_employeeIds", String.format("'%s'", ID.ToJoinedString(employeeIds)));

        return jdbcTemplate.getList(sql, new EmployeeDBRM());
    }
}
