package ru.pavloffsoft.progressis.core.services.personnel.employees.repositories;

import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDB;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public interface IEmployeesRepository {
    void add(EmployeeDB employeeDB);
    void edit(EmployeeDB employeeDB);
    void remove(ID employeeId, ID companyId);
    void delete(ID employeeId, ID companyId);

    EmployeeDB getEmployee(ID employeeId, ID companyId, boolean isRemoved);

    List<EmployeeDB> getEmployees(ID companyId, boolean isRemoved);
    List<EmployeeDB> getEmployees(List<ID> employeeIds);
}
