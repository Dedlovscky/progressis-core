package ru.pavloffsoft.progressis.core.services.personnel.repositories;

import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDB;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public interface IPersonnelRepository {
    void add(EmployeeDB employeeDB);
    void add(EmployeeDB employeeDB, UserDB userDB);
    void edit(EmployeeDB employeeDB);
    void edit(EmployeeDB employeeDB, UserDB userDB);

    void removeByEmployeeId(ID employeeId, ID companyId);
    void deleteByEmployeeId(ID employeeId, ID companyId);

    EmployeeDB getEmployee(ID employeeId, ID companyId, boolean isRemoved);
    UserDB getUser(ID employeeId, ID companyId, boolean isRemoved);

    List<EmployeeDB> getEmployees(ID companyId, boolean isRemoved);
    List<EmployeeDB> getEmployees(List<ID> employeeIds);

    List<UserDB> getUsers(List<ID> userIds);
    List<UserDB> getUsers(ID companyId, boolean isRemoved);
}
