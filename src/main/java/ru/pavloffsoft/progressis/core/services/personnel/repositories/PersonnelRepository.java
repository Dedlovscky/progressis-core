package ru.pavloffsoft.progressis.core.services.personnel.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDB;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.EmployeeDBRM;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Json;
import ru.pavloffsoft.progressis.core.tools.Sql;
import ru.pavloffsoft.progressis.core.tools.jdbc.IJdbc;

import java.util.List;
import java.util.regex.Matcher;

@Repository
public class PersonnelRepository implements IPersonnelRepository {
    @Autowired
    @Qualifier("resourceJdbcTemplate")
    private IJdbc jdbcTemplate;

    @Override
    public void add(EmployeeDB employeeDB) {
        String sql = Sql.read("/personnel/repositories/sql/employees_add.sql");

        sql = sql.replaceAll("p_id", String.format("'%s'", employeeDB.getId()));
        sql = sql.replaceAll("p_companyId", String.format("'%s'", employeeDB.getCompanyId()));
        sql = sql.replaceAll("p_lastName", String.format("'%s'", employeeDB.getLastName()));
        sql = sql.replaceAll("p_firstName", String.format("'%s'", employeeDB.getFirstName()));
        sql = sql.replaceAll("p_patronymic", String.format("'%s'", employeeDB.getPatronymic()));
        sql = sql.replaceAll("p_gender", String.format("%s", employeeDB.getGender().getNumber()));
        sql = sql.replaceAll("p_createdUserId", String.format("'%s'", employeeDB.getCreatedUserId()));
        sql = sql.replaceAll("p_createdDateTimeUTC", String.format("'%s'", employeeDB.getCreatedDateTimeUTC().toSqlString()));
        sql = sql.replaceAll("p_modifiedUserId", "NULL");
        sql = sql.replaceAll("p_modifiedDateTimeUTC", "NULL");
        sql = sql.replaceAll("p_isRemoved", "FALSE");

        jdbcTemplate.execute(sql);
    }

    @Override
    @Transactional
    public void add(EmployeeDB employeeDB, UserDB userDB) {
        String employee_sql = Sql.read("/personnel/repositories/sql/employees_add.sql");
        String user_sql = Sql.read("/personnel/repositories/sql/users_add.sql");

        employee_sql = employee_sql.replaceAll("p_id", String.format("'%s'", employeeDB.getId()));
        employee_sql = employee_sql.replaceAll("p_companyId", String.format("'%s'", employeeDB.getCompanyId()));
        employee_sql = employee_sql.replaceAll("p_lastName", String.format("'%s'", employeeDB.getLastName()));
        employee_sql = employee_sql.replaceAll("p_firstName", String.format("'%s'", employeeDB.getFirstName()));
        employee_sql = employee_sql.replaceAll("p_patronymic", String.format("'%s'", employeeDB.getPatronymic()));
        employee_sql = employee_sql.replaceAll("p_gender", String.format("%s", employeeDB.getGender().getNumber()));
        employee_sql = employee_sql.replaceAll("p_createdUserId", String.format("'%s'", employeeDB.getCreatedUserId()));
        employee_sql = employee_sql.replaceAll("p_createdDateTimeUTC", String.format("'%s'", employeeDB.getCreatedDateTimeUTC().toSqlString()));
        employee_sql = employee_sql.replaceAll("p_modifiedUserId", "NULL");
        employee_sql = employee_sql.replaceAll("p_modifiedDateTimeUTC", "NULL");
        employee_sql = employee_sql.replaceAll("p_isRemoved", "FALSE");

        user_sql = user_sql.replaceAll("p_id", String.format("'%s'", userDB.getId()));
        user_sql = user_sql.replaceAll("p_companyId", String.format("'%s'", userDB.getCompanyId()));
        user_sql = user_sql.replaceAll("p_employeeId", String.format("'%s'", userDB.getEmployeeId()));
        user_sql = user_sql.replaceAll("p_username", String.format("'%s'", userDB.getUsername()));
        user_sql = user_sql.replaceAll("p_password", String.format("'%s'", Matcher.quoteReplacement(userDB.getPassword())));
        user_sql = user_sql.replaceAll("p_role", String.format("%s", userDB.getRole().getNumber()));
        user_sql = user_sql.replaceAll("p_userGroupIds", String.format("'%s'", Json.stringify(userDB.getUserGroupIds())));
        user_sql = user_sql.replaceAll("p_createdUserId", String.format("'%s'", userDB.getCreatedUserId()));
        user_sql = user_sql.replaceAll("p_createdDateTimeUTC", String.format("'%s'", userDB.getCreatedDateTimeUTC().toSqlString()));
        user_sql = user_sql.replaceAll("p_modifiedUserId", "NULL");
        user_sql = user_sql.replaceAll("p_modifiedDateTimeUTC", "NULL");
        user_sql = user_sql.replaceAll("p_isRemoved", "FALSE");

        jdbcTemplate.execute(employee_sql);
        jdbcTemplate.execute(user_sql);
    }

    @Override
    public void edit(EmployeeDB employeeDB) {
        String sql = Sql.read("/personnel/repositories/sql/employees_edit.sql");

        sql = sql.replaceAll("p_id", String.format("'%s'", employeeDB.getId()));
        sql = sql.replaceAll("p_lastName", String.format("'%s'", employeeDB.getLastName()));
        sql = sql.replaceAll("p_firstName", String.format("'%s'", employeeDB.getFirstName()));
        sql = sql.replaceAll("p_patronymic", String.format("'%s'", employeeDB.getPatronymic()));
        sql = sql.replaceAll("p_gender", String.format("%s", employeeDB.getGender().getNumber()));
        sql = sql.replaceAll("p_modifiedUserId", String.format("'%s'", employeeDB.getModifiedUserId()));
        sql = sql.replaceAll("p_modifiedDateTimeUTC", String.format("'%s'", employeeDB.getModifiedDateTimeUTC().toSqlString()));

        jdbcTemplate.execute(sql);
    }

    @Override
    @Transactional
    public void edit(EmployeeDB employeeDB, UserDB userDB) {
        String employee_sql = Sql.read("/personnel/repositories/sql/employees_edit.sql");
        String user_sql = Sql.read("/personnel/repositories/sql/users_edit.sql");

        employee_sql = employee_sql.replaceAll("p_id", String.format("'%s'", employeeDB.getId()));
        employee_sql = employee_sql.replaceAll("p_lastName", String.format("'%s'", employeeDB.getLastName()));
        employee_sql = employee_sql.replaceAll("p_firstName", String.format("'%s'", employeeDB.getFirstName()));
        employee_sql = employee_sql.replaceAll("p_patronymic", String.format("'%s'", employeeDB.getPatronymic()));
        employee_sql = employee_sql.replaceAll("p_gender", String.format("%s", employeeDB.getGender().getNumber()));
        employee_sql = employee_sql.replaceAll("p_modifiedUserId", String.format("'%s'", employeeDB.getModifiedUserId()));
        employee_sql = employee_sql.replaceAll("p_modifiedDateTimeUTC", String.format("'%s'", employeeDB.getModifiedDateTimeUTC().toSqlString()));

        user_sql = user_sql.replaceAll("p_id", String.format("'%s'", userDB.getId()));
        user_sql = user_sql.replaceAll("p_username", String.format("'%s'", userDB.getUsername()));
        user_sql = user_sql.replaceAll("p_password", String.format("'%s'", userDB.getPassword()));
        user_sql = user_sql.replaceAll("p_role", String.format("%s", userDB.getRole().getNumber()));
        user_sql = user_sql.replaceAll("p_modifiedUserId", String.format("'%s'", userDB.getModifiedUserId()));
        user_sql = user_sql.replaceAll("p_modifiedDateTimeUTC", String.format("'%s'", userDB.getModifiedDateTimeUTC().toSqlString()));

        jdbcTemplate.execute(employee_sql);
        jdbcTemplate.execute(user_sql);
    }

    @Override
    @Transactional
    public void removeByEmployeeId(ID employeeId, ID companyId) {
        String employee_sql = Sql.read("/personnel/repositories/sql/employees_remove.sql");
        String user_sql = Sql.read("/personnel/repositories/sql/users_removeByEmployeeId.sql");

        employee_sql = employee_sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        employee_sql = employee_sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        user_sql = user_sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        user_sql = user_sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        jdbcTemplate.execute(employee_sql);
        jdbcTemplate.execute(user_sql);
    }

    @Override
    @Transactional
    public void deleteByEmployeeId(ID employeeId, ID companyId) {
        String employee_sql = Sql.read("/personnel/repositories/sql/employees_delete.sql");
        String user_sql = Sql.read("/users/repositories/sql/users_deleteByEmployeeId.sql");

        employee_sql = employee_sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        employee_sql = employee_sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        user_sql = user_sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        user_sql = user_sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        jdbcTemplate.execute(employee_sql);
        jdbcTemplate.execute(user_sql);
    }

    @Override
    public EmployeeDB getEmployee(ID employeeId, ID companyId, boolean isRemoved) {
        return null;
    }

    @Override
    public UserDB getUser(ID employeeId, ID companyId, boolean isRemoved) {
        return null;
    }

    @Override
    public List<EmployeeDB> getEmployees(ID companyId, boolean isRemoved) {
        String sql = Sql.read("/personnel/repositories/sql/employees_getByCompanyId.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_isRemoved", String.format("%s", isRemoved));

        return jdbcTemplate.getList(sql, new EmployeeDBRM());
    }

    @Override
    public List<EmployeeDB> getEmployees(List<ID> employeeIds) {
        return null;
    }

    @Override
    public List<UserDB> getUsers(List<ID> userIds) {
        return null;
    }

    @Override
    public List<UserDB> getUsers(ID companyId, boolean isRemoved) {
        return null;
    }
}
