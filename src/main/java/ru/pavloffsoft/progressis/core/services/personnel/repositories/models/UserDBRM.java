package ru.pavloffsoft.progressis.core.services.personnel.repositories.models;

import org.springframework.jdbc.core.RowMapper;
import ru.pavloffsoft.progressis.core.domain.users.types.Role;
import ru.pavloffsoft.progressis.core.tools.DateTime;
import ru.pavloffsoft.progressis.core.tools.ID;


import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDBRM implements RowMapper<UserDB> {
    @Override
    public UserDB mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserDB model = new UserDB();

        model.setId(ID.ToId(rs.getString("Id")));
        model.setCompanyId(ID.ToId(rs.getString("CompanyId")));
        model.setEmployeeId(ID.ToId(rs.getString("EmployeeId")));
//        List<Integer> list = new ArrayList<>();
//
//        try {
//            list = new ObjectMapper().readValue(rs.getString("Roles"), new TypeReference<List<Integer>>(){});
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        model.setRole(Role.getRole(rs.getInt("Role")));
        model.setUsername(rs.getString("Username"));
        model.setPassword(rs.getString("Password"));

        model.setCreatedUserId(ID.ToId(rs.getString("CreatedUserId")));
        model.setCreatedDateTimeUTC(new DateTime(rs.getString("CreatedDateTimeUTC")));
        model.setModifiedUserId(ID.ToId(rs.getString("ModifiedUserId")));
        model.setModifiedDateTimeUTC(new DateTime(rs.getString("ModifiedDateTimeUTC")));
        model.setRemoved(rs.getBoolean("IsRemoved"));

        return model;
    }
}
