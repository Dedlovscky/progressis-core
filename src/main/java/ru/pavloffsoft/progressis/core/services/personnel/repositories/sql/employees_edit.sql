UPDATE staff_employees SET
    LastName = p_lastName,
    FirstName = p_firstName,
    Patronymic = p_patronymic,
    Gender = p_gender,
    ModifiedUserId = p_modifiedUserId,
    ModifiedDateTimeUTC = p_modifiedDateTimeUTC
WHERE Id = p_id;