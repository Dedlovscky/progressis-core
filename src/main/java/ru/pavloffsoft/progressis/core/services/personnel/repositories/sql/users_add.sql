INSERT INTO personnel_users
(
    Id,
    CompanyId,
    EmployeeId,
    Username,
    Password,
    Role,
    UserGroupIds,
    CreatedUserId,
    CreatedDateTimeUTC,
    ModifiedUserId,
    ModifiedDateTimeUTC,
    IsRemoved
)
VALUES
(
    p_id,
    p_companyId,
    p_employeeId,
    p_username,
    p_password,
    p_role,
    p_userGroupIds,
    p_createdUserId,
    p_createdDateTimeUTC,
    p_modifiedUserId,
    p_modifiedDateTimeUTC,
    p_isRemoved
);