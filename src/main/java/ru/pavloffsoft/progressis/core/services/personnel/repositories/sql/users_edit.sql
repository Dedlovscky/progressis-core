UPDATE users SET
    Username = p_username,
    Password = p_password,
    Role = p_role,
    ModifiedUserId = p_modifiedUserId,
    ModifiedDateTimeUTC = p_modifiedDateTimeUTC
WHERE Id = p_id;