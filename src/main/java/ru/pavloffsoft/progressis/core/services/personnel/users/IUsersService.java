package ru.pavloffsoft.progressis.core.services.personnel.users;

import ru.pavloffsoft.progressis.core.domain.users.User;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public interface IUsersService {
    List<User> getUsers(List<ID> userIds);
}
