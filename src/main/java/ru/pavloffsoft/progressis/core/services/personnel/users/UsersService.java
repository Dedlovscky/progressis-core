package ru.pavloffsoft.progressis.core.services.personnel.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.core.domain.users.User;
import ru.pavloffsoft.progressis.core.services.personnel.converters.UsersConverter;
import ru.pavloffsoft.progressis.core.services.personnel.users.repositories.IUsersRepository;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService implements IUsersService {
    @Autowired
    private IUsersRepository usersRepository;

    @Override
    public List<User> getUsers(List<ID> userIds) {
        if(userIds.size() == 0) return new ArrayList<>(0);

        List<UserDB> userDBs = usersRepository.getUsers(userIds);

        return UsersConverter.convert(userDBs);
    }
}
