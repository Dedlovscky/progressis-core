package ru.pavloffsoft.progressis.core.services.personnel.users.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDB;
import ru.pavloffsoft.progressis.core.services.personnel.repositories.models.UserDBRM;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Sql;
import ru.pavloffsoft.progressis.core.tools.jdbc.IJdbc;

import java.util.List;

@Repository
public class UsersRepository implements IUsersRepository {

    @Autowired
    @Qualifier("authJdbcTemplate")
    private IJdbc jdbcTemplate;

    @Override
    public void add(UserDB userDB) {
        String sql = Sql.read("/users/repositories/sql/users_add.sql");
        sql = sql.replaceAll("p_id", String.format("'%s'", userDB.getId()));
        sql = sql.replaceAll("p_companyId", String.format("'%s'", userDB.getCompanyId()));
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", userDB.getEmployeeId()));
        sql = sql.replaceAll("p_username", String.format("'%s'", userDB.getUsername()));
        sql = sql.replaceAll("p_password", String.format("'%s'", userDB.getPassword()));
        sql = sql.replaceAll("p_role", String.format("%s", userDB.getRole().getNumber()));
        sql = sql.replaceAll("p_createdUserId", String.format("'%s'", userDB.getCreatedUserId()));
        sql = sql.replaceAll("p_createdDateTimeUTC", String.format("'%s'", userDB.getCreatedDateTimeUTC().toSqlString()));
        sql = sql.replaceAll("p_modifiedUserId", "NULL");
        sql = sql.replaceAll("p_modifiedDateTimeUTC", "NULL");
        sql = sql.replaceAll("p_isRemoved", "FALSE");

        jdbcTemplate.execute(sql);
    }

    @Override
    public void edit(UserDB userDB) {
        String sql = Sql.read("/users/repositories/sql/users_edit.sql");
        sql = sql.replaceAll("p_id", String.format("'%s'", userDB.getId()));
        sql = sql.replaceAll("p_username", String.format("'%s'", userDB.getUsername()));
        sql = sql.replaceAll("p_password", String.format("'%s'", userDB.getPassword()));
        sql = sql.replaceAll("p_role", String.format("%s", userDB.getRole().getNumber()));
        sql = sql.replaceAll("p_modifiedUserId", String.format("'%s'", userDB.getModifiedUserId()));
        sql = sql.replaceAll("p_modifiedDateTimeUTC", String.format("'%s'", userDB.getModifiedDateTimeUTC().toSqlString()));

        jdbcTemplate.execute(sql);
    }

    @Override
    public void removeById(ID userId, ID companyId){
        String sql = Sql.read("/users/repositories/sql/users_removeById.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_userId", String.format("'%s'", userId));

        jdbcTemplate.execute(sql);
    }

    @Override
    public void deleteById(ID userId, ID companyId){
        String sql = Sql.read("/users/repositories/sql/users_deleteById.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_userId", String.format("'%s'", userId));

        jdbcTemplate.execute(sql);
    }

    @Override
    public void removeByEmployeeId(ID employeeId, ID companyId){
        String sql = Sql.read("/users/repositories/sql/users_removeByEmployeeId.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        jdbcTemplate.execute(sql);
    }

    @Override
    public void deleteByEmployeeId(ID employeeId, ID companyId){
        String sql = Sql.read("/users/repositories/sql/users_deleteByEmployeeId.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));

        jdbcTemplate.execute(sql);
    }

    @Override
    public UserDB getUser(ID employeeId, boolean isRemoved) {
        String sql = Sql.read("/users/repositories/sql/users_getByEmployeeId.sql");
        sql = sql.replaceAll("p_employeeId", String.format("'%s'", employeeId));
        sql = sql.replaceAll("p_isRemoved", String.format("%s", isRemoved));

        return jdbcTemplate.get(sql, new UserDBRM());
    }

    @Override
    public List<UserDB> getUsers(List<ID> userIds) {
        String sql = Sql.read("/users/repositories/sql/users_getByUserIds.sql");
        sql = sql.replaceAll("p_userIds", String.format("'%s'", ID.ToJoinedString(userIds)));

        return jdbcTemplate.getList(sql, new UserDBRM());
    }

    @Override
    public List<UserDB> getUsers(ID companyId, boolean isRemoved) {
        String sql = Sql.read("/users/repositories/sql/users_getByCompanyId.sql");
        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_isRemoved", String.format("%s", isRemoved));

        return jdbcTemplate.getList(sql, new UserDBRM());
    }
}
