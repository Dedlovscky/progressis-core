package ru.pavloffsoft.progressis.core.services.usersGroups;

import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroup;
import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroupBlank;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import java.util.List;

public interface IUsersGroupsService {
    Response<Void> add(UserGroupBlank blank, SystemUser systemUser);
    Response<Void> edit(UserGroupBlank userGroup, SystemUser systemUser);
    Response<Void> remove(ID userGroupId, ID companyId);
    Response<Void> delete(ID userGroupId, ID companyId);

    UserGroup getUserGroup(ID userGroupId, ID companyId);
    List<UserGroup> getUsersGroups(ID companyId, boolean isRemoved);
}
