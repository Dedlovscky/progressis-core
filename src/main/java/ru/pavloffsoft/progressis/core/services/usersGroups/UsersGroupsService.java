package ru.pavloffsoft.progressis.core.services.usersGroups;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.pavloffsoft.progressis.core.domain.users.SystemUser;
import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroup;
import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroupBlank;
import ru.pavloffsoft.progressis.core.services.personnel.employees.IEmployeesService;
import ru.pavloffsoft.progressis.core.services.personnel.users.IUsersService;
import ru.pavloffsoft.progressis.core.services.usersGroups.converters.UsersGroupsConverter;
import ru.pavloffsoft.progressis.core.services.usersGroups.repositories.UsersGroupsRepository;
import ru.pavloffsoft.progressis.core.services.usersGroups.repositories.models.UserGroupDB;
import ru.pavloffsoft.progressis.core.tools.DateTime;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Response;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class UsersGroupsService implements IUsersGroupsService {
    @Autowired
    private UsersGroupsRepository usersGroupsRepository;
    @Autowired
    private IEmployeesService employeesService;
    @Autowired
    private IUsersService usersService;

    @Override
    public Response add(UserGroupBlank blank, SystemUser systemUser) {
        String message = validateUserGroupBlank(blank);

        if(!message.isBlank()) return new Response(false, message);

        UserGroupDB userGroupDB = new UserGroupDB(ID.NewId(), systemUser.getCompanyId(), blank.getName(), blank.getUserIds(),
                systemUser.getUserId(), DateTime.NowUTC());

        usersGroupsRepository.add(userGroupDB);

        return new Response<>(true, "Новая группа добавлена успешно");
    }

    @Override
    public Response edit(UserGroupBlank blank, SystemUser systemUser) {
        String message = validateUserGroupBlank(blank);

        if(!message.isBlank()) return new Response(false, message);

        UserGroupDB userGroupDB = usersGroupsRepository.getUserGroup(blank.getId(), systemUser.getCompanyId());

        if(userGroupDB == null) return new Response(false, "Группы не существует");

        userGroupDB.setName(blank.getName());
        userGroupDB.setUserIds(blank.getUserIds());
        userGroupDB.setModifiedUserId(systemUser.getUserId());
        userGroupDB.setModifiedDateTimeUTC(DateTime.NowUTC());

        usersGroupsRepository.edit(userGroupDB);

        return new Response(true, "Данные о группе сохранены успешно");
    }

    @Override
    public Response remove(ID userGroupId, ID companyId) {
        return null;
    }

    @Override
    public Response delete(ID userGroupId, ID companyId) {
        return null;
    }

    @Override
    public UserGroup getUserGroup(ID userGroupId, ID companyId) {
        if(userGroupId == null) return null;

        UserGroupDB userGroupDB = usersGroupsRepository.getUserGroup(userGroupId, companyId);

        if(userGroupDB == null) return null;

        return UsersGroupsConverter.convert(userGroupDB);
    }

    @Override
    public List<UserGroup> getUsersGroups(ID companyId, boolean isRemoved) {
        List<UserGroupDB> userGroupDBs = usersGroupsRepository.getUsersGroups(companyId, isRemoved);

        if (userGroupDBs.size() == 0) return new ArrayList<>(0);

        return UsersGroupsConverter.convert(userGroupDBs);
    }

    private String validateUserGroupBlank(UserGroupBlank userGroupBlank){
        if(userGroupBlank == null) return "Нет данных для сохранения";

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<UserGroupBlank>> result = validator.validate(userGroupBlank);

        for (ConstraintViolation<UserGroupBlank> validObject : result){
            if(!validObject.getMessage().isBlank()) return validObject.getMessage();
        }

        return "";
    }
}
