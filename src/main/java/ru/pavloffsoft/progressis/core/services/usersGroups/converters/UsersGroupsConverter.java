package ru.pavloffsoft.progressis.core.services.usersGroups.converters;

import ru.pavloffsoft.progressis.core.domain.usersGroups.UserGroup;
import ru.pavloffsoft.progressis.core.services.usersGroups.repositories.models.UserGroupDB;

import java.util.ArrayList;
import java.util.List;

public final class UsersGroupsConverter {
    public static UserGroup convert(UserGroupDB userGroupDB){
        return new UserGroup(userGroupDB.getId(), userGroupDB.getCompanyId(), userGroupDB.getName(), new ArrayList<>(0));
    }

    public static List<UserGroup> convert(List<UserGroupDB> userGroupDBs){
        List<UserGroup> userGroups = new ArrayList<>();

        for (UserGroupDB userGroupDB : userGroupDBs){
            userGroups.add(convert(userGroupDB));
        }

        return userGroups;
    }
}
