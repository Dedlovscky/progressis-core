package ru.pavloffsoft.progressis.core.services.usersGroups.repositories;

import ru.pavloffsoft.progressis.core.services.usersGroups.repositories.models.UserGroupDB;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public interface IUsersGroupsRepository {
    void add(UserGroupDB userGroupDB);
    void edit(UserGroupDB userGroupDB);
    void remove(ID userGroupId, ID companyId);
    void delete(ID userGroupId, ID companyId);

    UserGroupDB getUserGroup(ID userGroupId, ID companyId);
    List<UserGroupDB> getUsersGroups(ID companyId, boolean isRemoved);
}
