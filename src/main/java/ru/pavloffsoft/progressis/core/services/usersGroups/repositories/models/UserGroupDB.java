package ru.pavloffsoft.progressis.core.services.usersGroups.repositories.models;

import ru.pavloffsoft.progressis.core.tools.DateTime;
import ru.pavloffsoft.progressis.core.tools.ID;

import java.util.List;

public class UserGroupDB {
    private ID id;
    private ID companyId;
    private String name;
    private List<ID> userIds;

    private ID createdUserId;
    private DateTime createdDateTimeUTC;
    private ID modifiedUserId;
    private DateTime modifiedDateTimeUTC;
    private boolean isRemoved;

    public UserGroupDB(){}

    public UserGroupDB(ID id, ID companyId, String name, List<ID> userIds,
                       ID createdUserId, DateTime createdDateTimeUTC) {
        this.id = id;
        this.companyId = companyId;
        this.name = name;
        this.userIds = userIds;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = null;
        this.modifiedDateTimeUTC = null;
        this.isRemoved = false;
    }

    public UserGroupDB(ID id, ID companyId, String name, List<ID> userIds,
                       ID createdUserId, DateTime createdDateTimeUTC, ID modifiedUserId, DateTime modifiedDateTimeUTC, boolean isRemoved) {
        this.id = id;
        this.companyId = companyId;
        this.name = name;
        this.userIds = userIds;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = modifiedUserId;
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
        this.isRemoved = isRemoved;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public void setCompanyId(ID companyId) {
        this.companyId = companyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ID> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<ID> userIds) {
        this.userIds = userIds;
    }

    public ID getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(ID createdUserId) {
        this.createdUserId = createdUserId;
    }

    public DateTime getCreatedDateTimeUTC() {
        return createdDateTimeUTC;
    }

    public void setCreatedDateTimeUTC(DateTime createdDateTimeUTC) {
        this.createdDateTimeUTC = createdDateTimeUTC;
    }

    public ID getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(ID modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public DateTime getModifiedDateTimeUTC() {
        return modifiedDateTimeUTC;
    }

    public void setModifiedDateTimeUTC(DateTime modifiedDateTimeUTC) {
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }
}
