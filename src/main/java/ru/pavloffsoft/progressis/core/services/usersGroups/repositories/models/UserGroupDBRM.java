package ru.pavloffsoft.progressis.core.services.usersGroups.repositories.models;

import org.springframework.jdbc.core.RowMapper;
import ru.pavloffsoft.progressis.core.tools.DateTime;
import ru.pavloffsoft.progressis.core.tools.ID;
import ru.pavloffsoft.progressis.core.tools.Json;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserGroupDBRM implements RowMapper<UserGroupDB> {
    @Override
    public UserGroupDB mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserGroupDB model = new UserGroupDB();

        model.setId(ID.ToId(rs.getString("Id")));
        model.setName(rs.getString("Name"));
        model.setUserIds(Json.parse(rs.getString("UserIds")));

        model.setCreatedUserId(ID.ToId(rs.getString("CreatedUserId")));
        model.setCreatedDateTimeUTC(new DateTime(rs.getString("CreatedDateTimeUTC")));
        model.setModifiedUserId(ID.ToId(rs.getString("ModifiedUserId")));
        model.setModifiedDateTimeUTC(new DateTime(rs.getString("ModifiedDateTimeUTC")));
        model.setRemoved(rs.getBoolean("IsRemoved"));

        return model;
    }
}
