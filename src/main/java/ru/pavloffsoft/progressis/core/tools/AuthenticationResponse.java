package ru.pavloffsoft.progressis.core.tools;

import ru.pavloffsoft.progressis.core.domain.users.types.Role;

import java.util.List;

public class AuthenticationResponse {
    private ID userId;
    private ID companyId;
    private String accessToken;
    private String refreshToken;
    private List<Role> roles;

    public AuthenticationResponse(ID userId, ID companyId, String accessToken, String refreshToken, List<Role> roles) {
        this.userId = userId;
        this.companyId = companyId;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.roles = roles;
    }

    public ID getUserId() {
        return userId;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public List<Role> getRoles() {
        return roles;
    }
}
