package ru.pavloffsoft.progressis.core.tools;

import ru.pavloffsoft.progressis.core.domain.employees.EmployeeBlank;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

public class BlankModelValidator {
    public <TBlank> boolean isBlankModelValid(TBlank blank){
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<TBlank>> result = validator.validate(blank);

        for (ConstraintViolation<TBlank> validObject : result){
            if(!validObject.getMessage().isBlank()) return false;
        }

        return true;
    }
}
