package ru.pavloffsoft.progressis.core.tools;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class ID {
    private UUID id;

    public ID() {}

    public ID(UUID id) {
        this.id = id;
    }

    @JsonCreator
    public ID(String value) {
        this.id = convert(value);
    }

    public UUID getId() {
        return id;
    }

    public static ID NewId(){
        return new ID(UUID.randomUUID());
    }

    public static ID ToId(String value){
        return new ID(convert(value));
    }

//    public static List<ID> ToIds(String value){
//        List<ID> list = new ArrayList<>();
//
//        try {
//            list = new ObjectMapper().readValue(value, new TypeReference<>(){});
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return list;
//    }

    public static String ToJoinedString(List<ID> ids){
        return ids.stream().map(ID::toString).collect(Collectors.joining("','"));
    }

    private static UUID convert(String value){
        if(value == null || value.isBlank()) return null;

        UUID uuid;

        try {
            uuid = UUID.fromString(value);
        }catch (IllegalArgumentException e) {
            return null;
        }

        return uuid;
    }

    public String toSqlString() {
        return "\'" + id.toString() + "\'";
    }

    @JsonValue
    @Override
    public String toString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null) return  false;
        if(!(obj instanceof ID)) return false;

        return this.id.equals(((ID) obj).id);
    }
}
