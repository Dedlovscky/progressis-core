package ru.pavloffsoft.progressis.core.tools.jdbc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Jdbc implements IJdbc {

    private JdbcTemplate jdbcTemplate;

    public Jdbc(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void execute(String sql){
        jdbcTemplate.execute(sql);
    }

    @Override
    public <TResult, TRowMapper extends RowMapper> TResult get(String sql, TRowMapper rowMapper){
        List<TResult> models = jdbcTemplate.query(sql, rowMapper);

        return models.size() == 0 ? null : models.get(0);
    }

    @Override
    public <TResult, TRowMapper extends RowMapper> List<TResult> getList(String sql, TRowMapper rowMapper){
        return jdbcTemplate.query(sql, rowMapper);
    }
}
